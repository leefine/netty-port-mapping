# Netty Port Mapping

#### 介绍
基于Netty 4实现的端口映射工具，对于IT管理来说非常方便，有基于浏览器的在线管理界面，使用简单便捷，基于 Netty的NIO高性能的特性，该工具为端口映射提供了良好的网络性能,支持Windows和Linux,JDK1.8。

#### 软件架构
##### 1.基于JDK1.8
##### 2.应用层基于Spring Boot 2.7.14+Netty4.1.96
##### 3.前端基于Bootstrap4.6+Jquery3
##### 4.Windows/Centos（生产环境）


#### 使用注意
##### 1.支持HTTP
##### 2.支持TCP
##### 3.支持远程桌面、SSH
##### 4.数据库类测试暂时无法通过，改进中。
##### 5.首次启动后，请打开浏览器到http://localhost:9888 进行配置(默认账号test@163.com 密码 1@Proxy)
![输入图片说明](https://images.gitee.com/uploads/images/2021/0611/165512_a695c9fa_1375362.png "屏幕截图.png")
##### 6.默认配置数据使用JSON格式保存在程序根目录server.json中。

![输入图片说明](2.png)

##### 7.支持移动端的访问。

![输入图片说明](1.png)
