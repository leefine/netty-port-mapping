package com.leefine.nettysocketproxy.support;

import com.leefine.nettysocketproxy.config.AuthInterceptorConfig;

/**
 * Util
 *
 * @Author lihuai.chen@163.com
 * @Date 6/10/2021 1:09 PM
 */
public class Util {

    public static String getRootPath() {
        return System.getProperty("user.dir") + "/";
    }

    public static boolean checkToken(String token) {
        return token.equals(PasswordStrategy.encrypt(AuthInterceptorConfig.API_EMAIL + AuthInterceptorConfig.API_PASSWORD));
    }
}