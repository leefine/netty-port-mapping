package com.leefine.nettysocketproxy.support;

import org.apache.commons.codec.digest.DigestUtils;

/**
 * PasswordStrategy
 *
 * @Author lihuai.chen@163.com
 * @Date 9/10/2019 10:07 AM
 */
public final class PasswordStrategy {

    private PasswordStrategy() {
    }

    //系统默认，不要随意修改，如果系统上线后修改SALT会导致原密码失效
    private static final String SALT = "4565654666";
    //METHOD:md5_salt,sha1_salt,md5,sha1
    private static final String MD5_SALT = "md5_salt";
    private static final String SHA1_SALT = "sha1_salt";
    private static final String MD5 = "md5";
    private static final String SHA1 = "sha1";
    private static final String METHOD = SHA1;

    public static String encrypt(String text) {
        return encrypt(text, METHOD, SALT);
    }

    public static String encrypt(String text, String method, String salt) {
        switch (method) {
            case MD5_SALT:
                return md5(text + salt);
            case SHA1_SALT:
                return sha1(text + salt);
            case MD5:
                return md5(text);
            case SHA1:
                return sha1(text);
            default:
                return sha1(md5(text + salt));
        }
    }

    private static String md5(String text) {
        return DigestUtils.md5Hex(text);
    }

    private static String sha1(String text) {
        return DigestUtils.sha1Hex(text);
    }
}