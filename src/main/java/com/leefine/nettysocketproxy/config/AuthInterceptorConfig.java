package com.leefine.nettysocketproxy.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
/**
 * AuthInterceptorConfig
 *
 * Api Config , Auth email and Password
 *
 * @Author lihuai.chen@163.com
 * @Date 6/10/2021 11:59 AM
 */
@Configuration
public class AuthInterceptorConfig implements WebMvcConfigurer {

    @Value("${api.password}")
    private String password;

    @Value("${api.email}")
    private String email;

    @Value("${api.path}")
    private String path;

    public static String API_PASSWORD;
    public static String API_EMAIL;

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        API_PASSWORD = password;
        API_EMAIL = email;
        registry.addInterceptor(new AuthInterceptor()).addPathPatterns("/" + path+"/**").excludePathPatterns("/" + path+"/login");
    }
}