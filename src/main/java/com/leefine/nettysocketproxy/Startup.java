package com.leefine.nettysocketproxy;

import com.leefine.nettysocketproxy.proxy.SocketProxyServer;
import com.leefine.nettysocketproxy.support.SocketProxyServerJsonUtil;
import com.leefine.nettysocketproxy.support.ThreadHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.util.List;

/**
 * Startup
 *
 * Application Startup
 *
 * @Author lihuai.chen@163.com
 * @Date 6/10/2021 9:51 AM
 */
@SpringBootApplication
public class Startup {

    private final static Logger logger = LoggerFactory.getLogger(Startup.class);

    public static void main(String[] args) {
        SpringApplication.run(Startup.class, args);

        List<SocketProxyServer> serverList = SocketProxyServerJsonUtil.serverList();
        if (serverList != null && serverList.size() > 0) {
            for (SocketProxyServer socketProxyServer : serverList) {
                ThreadHelper.startThread(socketProxyServer);
            }
        } else {
            logger.error("There is no proxy,please goto 'http://localhost:9888' to set!");
        }
    }
}
