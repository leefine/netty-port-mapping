package com.leefine.nettysocketproxy.proxy;

import io.netty.channel.ChannelInitializer;
import io.netty.channel.socket.SocketChannel;
/**
 * SocketProxyInitializer
 *
 * @Author lihuai.chen@163.com
 * @Date 6/10/2021 11:59 AM
 */
public class SocketProxyInitializer extends ChannelInitializer<SocketChannel> {

    private final String remoteHost;
    private final int remotePort;

    public SocketProxyInitializer(String remoteHost, int remotePort) {
        this.remoteHost = remoteHost;
        this.remotePort = remotePort;
    }

    @Override
    public void initChannel(SocketChannel ch) {
        ch.pipeline().addLast(new SocketProxyFrontendHandler(remoteHost, remotePort));
    }
}